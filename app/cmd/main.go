package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
)

type Response struct {
	Message string `json:"message"`
}

func main() {
	db, err := sql.Open("mysql", "root:secret@tcp(172.26.0.1:3306)/prod")
	if err != nil {
		fmt.Println("Err open:", err.Error())
		return
	}

	err = db.Ping()
	if err != nil {
		fmt.Println("Err ping:", err.Error())
		return
	}

	defer db.Close()

	http.HandleFunc("/", HiHandleFunc)
	log.Fatal(http.ListenAndServe(":9000", nil))
}

func HiHandleFunc(w http.ResponseWriter, r *http.Request) {
	response := Response{
		Message: "Hi!",
	}

	jsonResponse, err := json.Marshal(response)
	if err != nil {
		log.Fatal(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonResponse)
}
