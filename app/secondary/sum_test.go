package secondary_test

import (
	"testing"

	"app.dev/secondary"
)

func TestSum(t *testing.T) {
	result := secondary.Sum(2, 3)
	expected := 5
	if result != expected {
		t.Errorf("Sum(2, 3) = %d; expected %d", result, expected)
	}
}
