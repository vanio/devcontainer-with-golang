# Template for Working with Golang 1.20 using Devcontainer in VSCode with Makefile

This is an example template for setting up a development environment with Golang 1.20 using Devcontainer in Visual Studio Code (VSCode) and a Makefile to automate common tasks. The source code related to this template can be found in the [devcontainer-with-golang](https://gitlab.com/vanio/devcontainer-with-golang) repository.

## Prerequisites

Before getting started, make sure the following software is installed on your machine:

- [Docker](https://www.docker.com/)
- [Visual Studio Code (VSCode)](https://code.visualstudio.com/)
- [Remote - Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) extension for VSCode (automatically installed when opening the project in VSCode)

## Setting Up the Development Environment

Follow the steps below to set up the development environment:

1. Clone or download the [devcontainer-with-golang](https://gitlab.com/vanio/devcontainer-with-golang) repository to your local machine.
2. Open the project directory in Visual Studio Code.
3. VSCode will automatically detect that the project is a Devcontainer and prompt you to build the development environment in Docker. Click on "Reopen in Container" to start the process.
4. VSCode will build the development environment based on the Dockerfile and devcontainer.json configurations provided in the project. This process may take a few minutes on the first run as Docker needs to download and set up the necessary dependencies.
5. Once the process is complete, VSCode will restart inside the Docker container. You are now ready to start developing in Golang.

## Using the Makefile

This template includes a Makefile that makes it easy to run common tasks. Some of the available tasks include:

- **make install**: download dependencies


## Contributing

If you would like to contribute to this project, please follow the steps below:

1. Fork the [devcontainer-with-golang](https://gitlab.com/vanio/devcontainer-with-golang) repository to your own GitLab account.
2. Clone the forked project to your local machine.
3. Create a branch for your changes: `git checkout -b name-branch`.
4. Make the desired changes and add meaningful commits.
5. Push your changes to GitLab: `git push origin name-branch`.
6. Open a Pull Request in the original repository.

## License

This template is distributed under the MIT License. See the [LICENSE](LICENSE) file for more information.

